class Router:

    def __init__(self, numNodo, tabla):
        self.numNodo = numNodo
        self.tablaRutas = []
        self.Tabla.subred = tabla.Subred
        self.Tabla.sgteSalto= tabla.sgteSalto
        self.Tabla.numSaltos = tabla.numSaltos
        self.tablaRutas.append(self.Tabla)
        self.vecinos = {}

    def ingresarVecinos(self, numNodo, dirIp):
        self.vecinos[numNodo] = dirIp
    
    def ingresarATabla(self, tabla):
        self.tablaRutas.append(tabla)


    class Tabla:
        def __init__(self, subred, sgteSalto, numSaltos):
            self.Subred.dirIp = subred.dirIp
            self.Subred.mascara = subred.mascara
            self.sgteSalto = sgteSalto
            self.numSaltos = numSaltos

        class Subred:
            def __init__(self, dirIp, mascara):
                self.dirIp = dirIp
                self.mascara = mascara




subred1 = Router.Tabla.Subred('10.0.1.0', '255.255.0.0')

tabla = Router.Tabla(subred1,'10.0.1.1', 1)

router1 = Router(1, tabla)

subred2 = Router.Tabla.Subred('10.0.2.0', '255.255.0.0')

tabla2 = Router.Tabla(subred2, '192.168.0.2', 3)

router1.ingresarATabla(tabla2)

router1.ingresarVecinos(3, '192.168.0.3')

print(router1.vecinos[3])